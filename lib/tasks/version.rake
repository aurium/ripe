require 'noosfero'
desc "shows noosfero version"
task :version do
  puts "noosfero, version #{Noosfero::VERSION}"
end
